# Spike Report

## SPIKE TITLE - C++ Basics

### Introduction

This spike report details how to create a simple guessing game and familiarise oneself with the
C++ interface while doing so.

### Goals

1. Become acquainted with C++ 
2. understand how it differs from C#
3. learn how to do basic input/output in C++
4. become more familiar with Visual Studio and it's debugging capabilities


### Personnel

* Primary - Matthew Louden
* Secondary - N/A

### Technologies, Tools, and Resources used

* [understanding the differences betwen C++ and C#] (https://csharp-station.com/understanding-the-differences-between-c-c-and-c/#:~:text=While%20C%2B%2B%20is%20an%20object,a%20component%2Doriented%20programming%20language.&text=C%2B%2B%20compiles%20into%20machine%20code,can%20automatically%20handle%20memory%20management.)
* [how to stop a program from exiting immediately] (https://stackoverflow.com/questions/2529617/how-to-stop-c-console-application-from-exiting-immediately#:~:text=Before%20the%20end%20of%20your,until%20you%20hit%20a%20key.&text=If%20you%20run%20your%20code,open%20when%20the%20application%20closes.)
* [how to create a number guessing game in C#] (https://www.youtube.com/watch?v=IhqdPDPV_g8)
* [how to create a number guessing game in C++] (https://www.youtube.com/watch?v=m0ByQy4fRqg)
* [how to create a random number generator with a range limiter] (https://stackoverflow.com/questions/40913780/generation-of-random-numberwithin-a-limit-code-overshooting-the-upper-limit)
* [how to detect whether an input variable is too high or too low] (http://www.cppforschool.com/assignment/library-functions-sol/guess-my-number.html)


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Generated New Visual Studio C++ Script
	1. [Learn basic functions of C++ scripting] (https://developers.google.com/edu/c++/)
	1. [Learn output and input functions] (https://www.youtube.com/watch?v=IhqdPDPV_g8)
2. Create a variable for the player's guess
	2. Create a function that generates a random number (using srand function)
	2. [create a range limiter for the former function] (https://stackoverflow.com/questions/40913780/generation-of-random-numberwithin-a-limit-code-overshooting-the-upper-limit)
	2. use cout << to print instructional text for the game onto the screen
	2. create a do loop
		2. inside do loop put the [detection system for whether the player's guess is too high or too low] (http://www.cppforschool.com/assignment/library-functions-sol/guess-my-number.html) and have it print a text prompt saying so.
		2. put a function at the end to [stop the game from immediately exiting upon guessing the right answer] (https://stackoverflow.com/questions/2529617/how-to-stop-c-console-application-from-exiting-immediately#:~:text=Before%20the%20end%20of%20your,until%20you%20hit%20a%20key.&text=If%20you%20run%20your%20code,open%20when%20the%20application%20closes.)

### What we found out

C++ allows for much easier screen printing than C#, in C++ it is either cout << or cin >> whereas with C# one would have to use Console.Writeline()