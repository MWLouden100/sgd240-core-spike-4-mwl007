//Author: Matthew Louden
//Program: Random Number Guessing Game

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
	// this generates the random number within the range, in C# this would be instantiating the random number generator
	// instead of having to use Random_random = new Random();
	int num, guess, tries = 0;
	srand(time(0)); 
	// this identifies the random number range
	// instead of public int RandomNumber (int min, int max); and then calling for the random number to return within the defined min/max range
	num = rand() % 20 + 1; 
	// this prints out the intro text, in C# this cout << would be written as Console.Writeline();.
	cout << "Random Number Guessing Game\n\n";
	cout << "(Enter a number then press the Enter key to check it)\n\n";

	do
	{
		cout << "Enter a guess between 1 and 20 : ";
		cin >> guess;
		tries++;

		if (guess > num)
			cout << "Too high!\n\n";
		else if (guess < num)
			cout << "Too low!\n\n";
		else
			cout << "\nCorrect! You got it in " << tries << " guesses!\n";
	} while (guess != num);

	system("pause");
	return 0;
}